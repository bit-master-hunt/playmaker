var appUser = undefined;
var appUrl = "http://138.49.184.122:3000/playmaker";

function titleCase( str ) {
    return str.replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();});
}

var clear = function( node ) {
   while( node.firstChild ) {
      node.removeChild( node.firstChild );
   }
}

var backButton = function( node, cb ) {
   var back = document.createElement('span');
   back.setAttribute('id', 'back');
   back.appendChild( document.createTextNode('<') );
   node.insertBefore( back, node.firstChild );
   back.addEventListener( 'click', cb );
}

var loginPage = function() {
   var content = document.getElementById('content');   
   clear( content );
   var page = '<div id="loginForm">Username: <input type="text" id="username"> <br/>Password: <input type="password" id="password"> <br/><input type="button" onclick="login()" value="Login">';
   content.innerHTML = page;
}

var fillGames = function( games ) {
   var div = document.getElementById('games');
   div.innerHTML = "<ul>" + games.map( function( game ) {
      return '<li>' + game.home.city + " " + game.home.name + " VS. " + game.away.city + " " + game.away.name + "</li>";
   } ).join('') + "</ul>";
}

var fillTeams = function( teams ) {
   var div = document.getElementById('teams');
   div.innerHTML = "<ul>" + teams.map( function( team ) {
      return '<li><span onclick="teamPage(\'' + team.id + '\')">' + team.city + " " + team.name + '</span></li>';
   } ).join('') + "</ul>";
}

var fillTeam = function( team ) {
   var div = document.getElementById('team');
   var headers = ['first', 'last', 'number', 'position' ];   
   var headerRow = '<tr>' + headers.map( function( header ) { return '<th>' + titleCase( header ) + '</th>'; } ).join('') + '</tr>';

   rows = team.players.map( function( player ) {
      return '<tr>' + headers.map( function( header ) { return '<td>' + player[ header ] + '</td>'; } ).join('') + '</tr>';
   } ).join('') 

  div.innerHTML = '<table>' + headerRow + rows + '</table>';
}

var teamPage = function( tid ) {
   var content = document.getElementById('content');
   clear( content );
   content.innerHTML = '<div id="teamPage"><div id="team"></div></div>';
   backButton( content, function( evt ) { homePage( appUser ); } );
   
   var url = appUrl + '/api/' + appUser.id + "/teams/" + tid + "?key=" + appUser.token;
   $.ajax( {
      method : 'get',
      url : url,
      success : fillTeam
   } );
}

var homePage = function( user ) {
   appUser = user;
   var content = document.getElementById('content');
   clear( content );
   var page = '<div id="homePage"><div id="teams"></div><div id="games"></div></div>';
   content.innerHTML = page;

   var url = appUrl + '/api/' + appUser.id + "/teams?key=" + appUser.token;
   $.ajax( {
      method : 'get',
      url : url,
      success : fillTeams
   } );

   var url = appUrl + '/api/' + appUser.id + "/games?key=" + appUser.token;
   $.ajax( {
      method : 'get',
      url : url,
      success : fillGames
   } );

}

var login = function() {
   var username = document.getElementById('username').value;
   var password = document.getElementById('password').value;
   var url = appUrl + '/login?username=' + username + '&password=' + password;
   $.ajax( {
      method : 'get',
      url : url,
      success : homePage
   } );
}
