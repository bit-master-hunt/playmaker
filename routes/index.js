var express = require('express');
var router = express.Router();

function guid() {
  function s4() {
    return Math.floor((1 + Math.random()) * 0x10000)
      .toString(16)
      .substring(1);
  }
  return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
    s4() + '-' + s4() + s4() + s4();
}

function repeat( f, n ) {
   var result = [];
   for( var i = 0 ; i < n ; i += 1 ) {
      result.push( f() );
   }
   return result;
}

var users = { };
var byId = { };
var db = { };

var Game = function( team1, team2, date, team1Score, team2Score ) {
   this.home = team1;
   this.away = team2;
   this.date = date || new Date();
   this.id = guid();
   this.score = { home : team1Score, away : team2Score };
   
   byId[ this.id ] = this;
}

var getGameSummary = function( game ) {
   var home = byId[ game.home ];
   var away = byId[ game.away ];
   
   return { id : game.id,
            home : { city : home.city, name : home.name, id : home.id },
            away : { city : away.city, name : away.name, id : away.id },
            score: game.score,
            date : game.date
          };
}

var Team = function( city, name, players ) {
   this.city = city;
   this.name = name;
   this.players = players;
   this.id = guid();

   byId[ this.id ] = this;
}

var getTeamSummary = function( team ) {
   return { city : team.city, name : team.name, id : team.id };
};

var Player = function( first, last, number, position, status ) {
   this.first = first;
   this.last = last;
   this.number = number
   this.status = status;
   this.position = position;
   this.id = guid();

   byId[ this.id ] = this;
}

var uid = 1;
var userId = function( ) { return uid ++; }
var User = function( username, password ) {
   this.username = username;
   this.password = password || '123';
   this.id = userId();

   byId[ this.id ] = this;
   users[ this.username ] = this;
}

var choose = function( vals ) {
   return vals[ Math.floor( Math.random( ) * vals.length ) ];
};

var randomScore = function( ) {
   return Math.floor( Math.random() * 45 );
};

var randomPlayer = function() {
   var firsts = ["Kenny", "Deantye", "Tyrell", "Nathaniel", "Bob", "William", "John", "Doug", "Martin", "Matthew", "Justin", "Joshua", "Fred", "Kent", "Adam", "Thomas", "Eddie", 'Todd' ];
   var lasts = ['Roders', 'Adams', 'Lacy', 'Nelson', 'Matthews', 'Hunt', 'Davis', 'Williams', 'McBride', 'Brady', 'Manning', 'Gurley', 'McShay', 'Forde', 'Langham', 'Beckham' ];
   var statuss = ['Senior', 'Senior', 'Junior', 'Senior', 'Junior', 'Sophomore', 'Freshman' ];
   var positions = ['RB', 'QB', 'WR', 'TE', 'OLB', 'OT', 'OG', 'C', 'FB', 'CB', 'S' ];
   
   return new Player( choose( firsts ), choose( lasts ), Math.floor( Math.random() * 90 + 1 ), choose( positions ), choose( statuss ) );
}

var randomTeam = function( ) {
   var cities = ["La Crosse", "Sparta", "Onalaska", "La Crescent", "Chicago", "Tomah", "Trempeleau", "Ettrick", "West Salem", "Caledonia", "Houston", "St. Louis", "Peoria", "Madison", "Milwaukee"];
   var names = ["Red Raiders", "Vikings", "Huskies", "Dolphins", "Golden Eagles", "Screaming Eagles", "Night Hawks", "Lightning", "Hilltoppers", "Jumping Beans", "Cyclers" ];
   return new Team( choose( cities ), choose( names ), repeat( randomPlayer, 20 ) );
}

var randomGame = function( teams )  {
   var home = choose( teams );
   var away = choose( teams );
   while( home.id === away.id ) {
      away = choose( teams );
   }

   return new Game( home.id, away.id, new Date(), randomScore(), randomScore() );
};


var users = function( ) {
   var names = ["jemimah", "jerusalem", "jeremy", "jumbotron", "jordan", "jamboree", "jackson", "jim", "james", "jonathan", "jotham", "jimbeam", "jackdaniels", "jason", "jbsmith", "jill", "jimmy", "jolene" ];
   return names.map( function(name) { return new User(name); }  );
}

// I'm using this for authentication.  Can't do it right because of CORS restrictions on local filesystem javascript
tokens = {};

router.all( '/api/:uid/*', function( req, res, next ) {
   var token = req.query.key;
   if( token && tokens[ req.params.uid ] == token ) {
      next();
   } else {
      res.status( 401 ).json({});
   }
} );

var inited = false;
router.get('/create', function( req, res, next ) {
   if( inited ) return;
   inited = true;
   
   var userIds = users();
   userIds.forEach( function( user ) {
      var teams = repeat( randomTeam, Math.floor( Math.random() * 5 + 2 ) );
      var games = repeat( function() { return randomGame( teams ); }, Math.floor( Math.random() * 4 + 3 ) );
      db[ user.id ] = { games : games, teams : teams }
   } );

   res.json( db );
} );


var authenticates = function( credentials ) {
   return users[ credentials.username ] &&  users[ credentials.username ].password == credentials.password;
};

var user = function( credentials ) {
   var user = users[ credentials.username ];
   return { username : user.username, id : user.id }; // strip out password
};



router.get( '/login', function( req, res, next ) {
   var credentials = { username : req.query.username, password : req.query.password };
   if( authenticates( credentials ) ) {
      var token = guid();
      var user = users[ credentials.username ];      
      tokens[ user.id ] = token;
      res.json( { username : user.username, id : user.id, token : token } );
   } else {
      res.json( null );
   }
} );

router.get('/api/:uid/games', function(req, res, next) {
   res.json( db[ req.params.uid ].games.map( getGameSummary ) );
});

router.post('/api/:uid/games', function( req, res, next ) {
   var home = req.body.home;
   var away = req.body.away;
   var date = req.body.date;
   var awayScore = req.body.awayScore;
   var homeScore = req.body.homeScore;
   var game = new Game( home, away, date, homeScore, awayScore );
   db[ req.params.uid ].games.push( game );
   res.json( game );
} );

router.get('/api/:uid/games/:gid', function(req, res, next) {
   res.json( byId[ req.params.gid ] );
});

router.delete('/api/:uid/games/:gid', function(req, res, next) {
   var games = db[ req.params.uid ].games;
   var game = byId[ req.params.gid ];
   delete byId[ req.params.gid ];

   db[ req.params.uid ].games = db[ req.params.uid ].games.filter( function( g ) { return g.id !== game.id; } );
   res.json( game );
});

router.get('/api/:uid/teams', function(req, res, next) {
   res.json( db[ req.params.uid ].teams.map( getTeamSummary ) );
});

router.get('/api/:uid/teams/:tid', function(req, res, next) {
   res.json( byId[ req.params.tid ] );
});

router.get('/api/:uid/teams/:tid/players', function(req, res, next) {
   res.json( byId[ req.params.tid ].players );
});

router.get('/api/:uid/teams/:tid/players/:pid', function(req, res, next) {
   res.json( byId[ req.params.pid ] );
});

router.post('/api/:uid/teams/:tid/players', function(req, res, next) {
   var first = req.body.first;
   var last = req.body.last;
   var number = req.body.number;
   var status = req.body.status;
   var position = req.body.position;

   var player = new Player( first, last, number, position, status );
   byId[ req.params.tid ].players.push( player );
   res.json( player );
});

router.delete('/api/:uid/teams/:tid/players/:pid', function(req, res, next) {
   var player = byId[ req.params.pid ];
   var team = byId[ req.params.tid ];
   
   team.players = team.players.filter( function( p ) { return p.id !== player.id } );
   delete byId[ req.params.pid ];
   res.json( player );
});

module.exports = router;
